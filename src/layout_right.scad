include <../keyboard_lib/keyboard.scad>
include <../Round-Anything/polyround.scad>

module right_case() {
    offset(2){
        translate([-1.875,-6.775]){
            import("layout_right_case.svg");
        }
    }
}

circuits = [
    [0.06,2.23,0,-20,"ec11_double",["none","none","none","none"]],
    [0.42,0,0,0,"pro_micro"],
    [-0.11,0,0,0,"trrs"],
    [2.2,2.65,0,0,"manual_trace", [[0,0],[-20,0],[-21,-2]], "bottom"],
    [3.2,2.53,0,0,"manual_trace", [[0,0],[-10,0],[-21,6],[-44,16]], "bottom"],
    [4.2,2.41,0,0,"manual_trace", [[0,0],[-10,0],[-21,6],[-44,16]], "bottom"],
    [5.2,2.53,0,0,"manual_trace", [[0,0],[-10,0],[-21,2],[-38,9]], "bottom"],
    [6.2,2.65,0,0,"manual_trace", [[0,0],[-10,0],[-38,5]], "bottom"],
    [7.2,2.65,0,0,"manual_trace", [[0,0],[-10,0],[-38,9]], "bottom"],
    [0.58,2.63,0,0,"manual_trace", [[0,0],[2,16]], "top"],
    [0.85,3.4,0,0,"manual_trace", [[0,0],[19,-2.3]], "top"],
    [2.05,3.25,0,0,"manual_trace", [[0,0],[10,-3.3],[18,-3.3]], "top"],
];

layout = [
    [[1,1.453,0,[],["none","connect","connect","none"]],[1,0,-0.125,[],["none","connect","connect","connect"]],[1,0,-0.125,[],["none","connect","connect","connect"]],[1,0,0.125,[],["none","connect","connect","connect"]],[1,0,0.125,[],["none","connect","connect","connect"]],[1,0,0,[],["none","connect","none","connect"]]],
    [[1,1.453,0,[],["connect","connect","connect","none"]],[1,0,-0.125,[],["connect","connect","connect","connect"]],[1,0,-0.125,[],["connect","connect","connect","connect"]],[1,0,0.125,[],["connect","connect","connect","connect"]],[1,0,0.125,[],["connect","connect","connect","connect"]],[1,0,0,[],["connect","connect","none","connect"]]],
    [[1,1.453,0,[],["connect","none","connect","none"]],[1,0,-0.125,[],["connect","none","connect","connect"]],[1,0,-0.125,[],["connect","none","connect","connect"]],[1,0,0.125,[],["connect","none","connect","connect"]],[1,0,0.125,[],["connect","none","connect","connect"]],[1,0,0,[],["connect","none","none","connect"]]],
    [[1,2.453,-0.125,[],["none","none","connect","none"]],[1,0,-0.125,[],["none","none","connect","connect"]],[1,0,0.125,[],["none","none","none","connect"]]],
    [[1,0.07,-0.57,[-20],["none","none","none","none"]],[1,0.2,-0.26,[-10],["none","none","none","none"]]]
];

module right_board_smooth_f(depth=5) {
    difference() {
        board(layout, circuits=circuits, depth=depth, thickness=30){
            right_case();
        }
        rotate([1,7,0]) {
            translate([-100,-100, 20.3]) {
                cube([1000,1000,100]);
            }
        }
    }
}

module right_board_smooth() { // make stl
    right_board_smooth_f();
}

module left_board_rounded() { // make stl
    intersection() {
        right_board_smooth_f(depth=5);
        translate([0,0, -5-5])
            extrudeWithRadius(45+5,r1=4,r2=0,fn=30){
                offset(5)
                    right_case();
        }
    }
}

module right_board_apollo() { // make stl
    intersection(){
        scale(0.25)
            translate([675,-280,-85])
                rotate([0,180])
                    import("../assets/craters_1.stl");
        right_board_smooth_f(depth=19);
        translate([0,0, -5-16])
            extrudeWithRadius(45+16,r1=16,r2=0,fn=60){
                offset(5)
                    right_case();
            }
    }
    right_board_smooth_f(depth=0);
}

module right_plate_drawing() { // make svg
    top_plate_drawing(layout, circuits=circuits){
        right_case();
    }
}

module right_plate() { // make stl
    top_plate(layout, circuits=circuits){
        right_case();
    }
}

module right_pcb_traces_top() { // make svg
    pcb_traces_top(layout, circuits=circuits){
        right_case();
    }
}

module right_pcb_traces_bottom() { // make svg
    pcb_traces_bottom(layout, circuits=circuits){
        right_case();
    }
}

module right_pcb_drill() { // make svg
    pcb_drill(layout, circuits=circuits, single=false){
        right_case();
    }
}

module right_pcb_render() {
    pcb_render(layout, circuits=circuits, single=false) {
        right_case();
    }
}

right_apollo_cube();