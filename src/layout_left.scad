include <../keyboard_lib/keyboard.scad>
include <../Round-Anything/polyround.scad>

module left_case() {
    offset(2){
        translate([-1.875,-6.775]){
            import("layout_left_case.svg");
        }
    }
}

circuits = [
    [6.1,0,0,0,"pro_micro", "none"],
    [7.25,0,0,0,"trrs"],
    // Column -> R4 Connections
    [5.75,2.64,0,0,"manual_trace", [[0,0],[17,0],[22.2,2]], "bottom"],
    [4.75,2.52,0,0,"manual_trace", [[0,0],[12,7],[35,7],[40,15.5]], "bottom"],
    [3.75,2.40,0,0,"manual_trace", [[0,0],[10,7],[23,7],[36,15]], "bottom"],
    [2.75,2.52,0,0,"manual_trace", [[0,0],[10,2],[18,2],[32.5,8.5]], "bottom"],
    [1.75,2.95,0,0,"manual_trace", [[0,-6],[18,-1.25],[32,-2]], "bottom"],
    [0.75,2.95,0,0,"manual_trace", [[0,-6],[0,2],[10,2],[18,1.25],[32.5,0.5]], "bottom"],
    // R4 Row Connection
    [1,0.65,0,0,"manual_trace", [[0,0],[0,19.05]], "bottom", "holes"],
    [1,1.65,0,0,"manual_trace", [[0,0],[0,19.05]], "bottom", "holes"],
    [1.1,0.95,0,0,"manual_trace", [[0,0],[0,19.05]], "bottom", "holes"],
    [1.1,1.95,0,0,"manual_trace", [[0,0],[0,19.05]], "bottom", "holes"],
    // Row Connection Thumb Cluster
    [4.78,3.1,0,0,"manual_trace", [[-0.5,-0.3],[17,3.8]], "top"],
    [4.78,3.55,0,0,"manual_trace", [[-0.5,-0],[11,3]], "top"],
    [4.75,3.65,0,0,"manual_trace", [[-0.5,-0.3],[11,3]], "top"],
    [4.75,3.85,0,0,"manual_trace", [[-0.5,-0.3],[11,3]], "top"],
    [5.95,3.38,0,0,"manual_trace", [[-0.5,-0.7],[6.5,0],[16,1],[18,2]], "top"],
    [5.83,3.8,0,0,"manual_trace", [[0,0],[12,0.3]], "top"],
    [5.8,3.87,0,0,"manual_trace", [[0,0],[12,0.3]], "top"],
    [5.77,4.08,0,0,"manual_trace", [[0,0],[11,0.3]], "top"],
    [5.73,2.96,0,0,"manual_trace", [[0,0],[12,-1.3]], "top"],
    [5.73,2.78,0,0,"manual_trace", [[0,0],[13.5,-1.7]], "top"],
    [5.72,2.67,0,0,"manual_trace", [[0,0],[14,-1]], "top"],
    [7.1,2.4,0,0,"manual_trace", [[-0.5,-1],[1,2],[-0.5,22]], "top"],
    // Wire Digital Pin RGB
    [0.25,0.75,0,0,"manual_trace", [[0,0],[-2,0],[-2,19.05],[0,19.05]], "top"],
    [5.76,1.75,0,0,"manual_trace", [[0,0],[4,0],[4,16],[13,15.5],[23.5,19.2],[22.7,21.5],[21.5,21]], "top"],
    [0.25,2.75,0,0,"manual_trace", [[0,0],[-1,0],[-1,7],[19.05*2,19.05*.875]], "top"],
    [5.76,0.76,0,0,"manual_trace", [[0,0],[2,0]], "top"],
    [5.76,0.76,0,0,"manual_trace", [[2,0],[5,0],[5,-10.5],[7,-10.5]], "bottom","hole_begin"],
    // Row -> Pro Micro Wiring
    [5.76,0.21,0,0,"manual_trace", [[0,0],[4,0],[4,20],[8,20]], "top"],
    [5.76,1.21,0,0,"manual_trace", [[0,0],[3,0],[3,3.7],[8,3.7]], "top"],
    [5.76,2.21,0,0,"manual_trace", [[0,0],[3,0]], "top"],
    [5.76,2.21,0,0,"manual_trace", [[2,0],[6,0]], "bottom","holes"],
    [5.76,2.21,0,0,"manual_trace", [[6,0],[6,-13],[8,-13]], "top"],
    [6.86,2.27,0,0,"manual_trace", [[0,0],[-4,-2],[-13,-2],[-13,-12]], "top"],
    // Pro Micro -> TRRS
    [7,0.33,0,0,"manual_trace", [[0,0],[9,0]], "bottom"],
    [7,0.6,0,0,"manual_trace", [[0,0],[5,0]], "bottom"],
    [7.1,0.1,0,0,"manual_trace", [[7,2],[4,2],[4,0],[-15.5,0],[-15.5,9.5],[-18,9.5]], "top"],
    // RGB
    [7,0.33,0,0,"manual_trace", [[0,0],[-4,0],[-4,33],[5,37.5],[2,70],[-1.5,69.5]], "top"],
    [7,0.6,0,0,"manual_trace", [[0,0],[-2.5,0],[-2.5,27],[6.5,31.5],[3,68],[-3.5,69.5]], "top"],
    // Column -> Pro Micro
    [7,1,0,0,"manual_trace", [[0,0],[-3,0],[-3,-18],[-64,-23],[-74,-23],[-121,-17]], "bottom"],
    [7,1.13,0,0,"manual_trace", [[0,0],[-12,0],[-12,16.5],[-3,20]], "bottom"],
    [7,1.26,0,0,"manual_trace", [[0,0],[-11,0],[-11,13],[3,16.5],[3,45],[2,44]], "bottom"],
    [7,1.39,0,0,"manual_trace", [[0,0],[-10,0],[-10,9.5],[4,13],[4,44],[-2,52],[-6,52],[-22.5,45]], "bottom"],
    [7,1.52,0,0,"manual_trace", [[0,0],[-9,0],[-9,6],[5,9.5],[5,42],[-1.5,50.5],[-6,50.5],[-43,43],[-43,35]], "bottom"],
    [7,1.65,0,0,"manual_trace", [[0,0],[-8,0],[-8,2.5],[6,6],[6,40],[-1,49],[-7,49],[-62,38],[-62.1,33]], "bottom"],
];

layout = [
    [[1,0,0,[],["none","connect","connect","none"]],[1,0,0,[],["none","connect","connect","connect"]],[1,0,-0.125,[],["none","connect","connect","connect"]],[1,0,-0.125,[],["none","connect","connect","connect"]],[1,0,0.125,[],["none","connect","connect","connect"]],[1,0,0.125,[],["none","connect","none","connect"]]],
    [[1,0,0,[],["connect","connect","connect","none"]],[1,0,0,[],["connect","connect","connect","connect"]],[1,0,-0.125,[],["connect","connect","connect","connect"]],[1,0,-0.125,[],["connect","connect","connect","connect"]],[1,0,0.125,[],["connect","connect","connect","connect"]],[1,0,0.125,[],["connect","connect","none","connect"]]],
    [[1,0,0,[],["connect","none","connect","none"]],[1,0,0,[],["connect","none","connect","connect"]],[1,0,-0.125,[],["connect","none","connect","connect"]],[1,0,-0.125,[],["connect","none","connect","connect"]],[1,0,0.125,[],["connect","none","connect","connect"]],[1,0,0.125,[],["connect","none","none","connect"]]],
    [[1,2,-0.125,[],["none","none","connect","none"]],[1,0,-0.125,[],["none","none","connect","connect"]],[1,0,0.125,[],["none","none","none","connect"]],[1,0.2,0.125,[10],["none","none","none","none"]],[1,0.25,0.1,[20],["none","none","none","none"]],[1,-1,-1.2,[20],["none","none","none","none"]]]
];

module left_board_smooth_f(depth=5) {
    difference() {
        board(layout, circuits=circuits, depth=depth, thickness=30){
            left_case();
        }
        rotate([1,-7,0]) {
            translate([-100,-100, 3]) {
                cube([1000,1000,100]);
            }
        }
    }
}

module right_board_smooth() { // make stl
    right_board_smooth_f();
}

module left_board_rounded() { // make stl
    intersection() {
        left_board_smooth_f(depth=5);
        translate([0,0, -5-5])
            extrudeWithRadius(45+5,r1=4,r2=0,fn=30){
                offset(5)
                    left_case();
        }
    }
}

module left_board_apollo() { // make stl
    intersection(){
        scale(0.41)
            translate([374,-165,-45])
                rotate([0,180])
                    import("../assets/craters_2.stl");
        render()
            left_board_smooth_f(depth=16);
        render()
            translate([0,0, -5-16])
                extrudeWithRadius(45+16,r1=16,r2=0,fn=60){
                    offset(5)
                        left_case();
        }
    }
    left_board_smooth_f(0);
}

module left_plate_drawing() { // make svg
    top_plate_drawing(layout, circuits=circuits){
        left_case();
    }
}

module left_plate() { // make stl
    top_plate(layout, circuits=circuits){
        left_case();
    }
}

module left_pcb_traces_top() { // make svg
    pcb_traces_top(layout, circuits=circuits){
        left_case();
    }
}

module left_pcb_traces_bottom() { // make svg
    pcb_traces_bottom(layout, circuits=circuits){
        left_case();
    }
}

module left_pcb_drill() { // make svg
    pcb_drill(layout, circuits=circuits, single=false){
        left_case();
    }
}

module left_pcb_render() {
    pcb_render(layout, circuits=circuits, single=false) {
        left_case();
    }
}
left_board_apollo();
// left_plate_drawing();
// left_pcb_traces_top();
// left_pcb_traces_bottom();
// left_pcb_drill();
// left_pcb_render();