left_targets_stl = $(shell grep '// make stl' src/layout_left.scad | sed -e 's/left_//g' -e 's/module /build\/left_/g' -e 's/().*/.stl/g')
right_targets_stl = $(shell grep '// make stl' src/layout_right.scad | sed -e 's/right_//g' -e 's/module /build\/right_/g' -e 's/().*/.stl/g')
left_targets_svg = $(shell grep '// make svg' src/layout_left.scad | sed -e 's/left_//g' -e 's/module /build\/left_/g' -e 's/().*/.svg/g')
right_targets_svg = $(shell grep '// make svg' src/layout_right.scad | sed -e 's/right_//g' -e 's/module /build\/right_/g' -e 's/().*/.svg/g')
all_targets = ${left_targets_stl} ${right_targets_stl} ${left_targets_svg} ${right_targets_svg}
upload_all_targets = $(shell echo ${all_targets} | sed 's,\(build\),upload,g')
asset_all_targets = $(shell echo ${all_targets} | sed 's,build/\([^ ]*\),--assets-link \"{\\"name\\"\:\\"\1\\"\,\\"url\\":\\"${package_registry_url}/\1\\"}\" ,g')

git_tag = $(shell git describe --tags)
components_tgz = mittens_components.${git_tag}.tgz

# Release variables
package_registry_url=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/mittens_kb_assets/${git_tag}
changelog_url=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/changelog
changelog_anchor=$(shell echo ${git_tag} | sed 's,\.,,g')-$(shell date +"%Y-%m-%d")
changelog_link=${CI_PROJECT_URL}/-/blob/main/CHANGELOG.md\#${changelog_anchor}

define release_description
### Changes

For changes, take a look at the [changelog entry for ${git_tag}](${changelog_link}).

### Usage

Download, slice, and print the stls, svg/dxf files are intended for cutting with a CNC.
endef

build: ${all_targets}

print:
	@echo All targets:
	@for target in ${all_targets}; do echo $$target; done

tar:
	@mv build mittens_components
	@tar czf ${components_tgz} mittens_components
	@mv mittens_components build

clean:
	@echo removing build and tmp folders
	@rm -rf tmp build

build/left_%.svg:
	@mkdir -p tmp
	@mkdir -p build
	@echo 'include <../mittens.scad>\n\nleft_$*();' > tmp/left_$*.svg.scad
	@echo Building $@
	openscad tmp/left_$*.svg.scad -o $@
	@rm tmp/left_$*.svg.scad

build/left_%.stl:
	@mkdir -p tmp
	@mkdir -p build
	@echo 'include <../mittens.scad>\n\nleft_$*();' > tmp/left_$*.stl.scad
	@echo Building $@
	openscad tmp/left_$*.stl.scad -o $@
	@rm tmp/left_$*.stl.scad

build/right_%.svg:
	@mkdir -p tmp
	@mkdir -p build
	@echo 'include <../mittens.scad>\n\nright_$*();' > tmp/right_$*.svg.scad
	@echo Building $@
	openscad tmp/right_$*.svg.scad -o $@
	@rm tmp/right_$*.svg.scad

build/right_%.stl:
	@mkdir -p tmp
	@mkdir -p build
	@echo 'include <../mittens.scad>\n\nright_$*();' > tmp/right_$*.stl.scad
	@echo Building $@
	openscad tmp/right_$*.stl.scad -o $@
	@rm tmp/right_$*.stl.scad

sca2d:
	sca2d --gitlab-report src/

export release_description
release.description:
	@echo "$$release_description" > description.txt

upload/%:
	curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file build/$* "${package_registry_url}/$*"

release.packages: ${upload_all_targets}

release: release.description release.packages
		curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ${components_tgz} "${package_registry_url}/${components_tgz}"
		curl --header "PRIVATE-TOKEN: ${CHANGELOG_TOKEN}" --data "version=${CI_COMMIT_TAG}" ${changelog_url}
		release-cli create --name "Version ${CI_COMMIT_TAG}" --tag-name ${CI_COMMIT_TAG} \
			--assets-link "{\"name\":\"${components_tgz}\",\"url\":\"${package_registry_url}/${components_tgz}\"}" \
			${asset_all_targets} \
			--description description.txt
